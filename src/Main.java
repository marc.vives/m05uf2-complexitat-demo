/**
 * Classe per treballar els diagrames de flux i la complexitat ciclomatica
 * V(G) = arestes - nodes + 2
 * V(G) = n. predicats + 1
 * V(G) = regions
 */

public class Main {

    public static void main() {
        doSomthing();       //1
        doSomthingElse();   //2
    }

    public static void doSomthing(){}
    public static void doSomthingElse(){}


    static void exempleIf(boolean a){

        if(a){
            doSomthing();
        }

    }

    static void exempleIfElse(boolean a){

        if(a){
            doSomthing();
        }else{
            doSomthingElse();
        }

    }


    static void exempleIfAND(boolean a, boolean b){

        if(a && b){
            doSomthing();
        }

    }



    static void exempleIfOR(boolean a, boolean b){

        if(a || b){
            doSomthing();
        }

    }

    static void exempleWhile(boolean a){

        while(a){
            doSomthing();
        }

    }


    static void exempleDoWhile(boolean a){

        do{
            doSomthing();
        }while(a);

    }


    static void exempleSwhitch(int a){

        switch(a){
            case 1: doSomthing(); break;
            case 2: doSomthingElse();
            case 3: doSomthing(); break;
        }

    }


    static void exempleCompost(boolean a, boolean b, boolean c){
        if (a && b){
            doSomthing();
        }else if (c){
            doSomthingElse();
        }
    }

    static int exempleCompost2(boolean a, boolean b, boolean c){
        int resultat = 0;
        if (a && b){
            resultat = 1;
        }else if (c){
            resultat = 2;
        }
        return resultat;
    }
}
