public class Activitats {


    public int num_major(int a,int b,int c){
        if (a>b && a>c){
            return a;
        } else if (c>b){
            return c;
        } else {
            return b;
        }
    }


    public int a(int a, int b, int x) {
        if(a > 0 && b==0) {
            x = x / a;
        }
        if(a==2 || x > 1) {
            x = x+1;
        }
        return x;
    }

    public void ActivitatSwithOperation(int operation, int number1, int number2){
        switch(operation){
            case 1:
                System.out.println("multiplicar()");
                break;
            case 2:
                if(number2 != 0) {
                    System.out.println("dividir()");
                    break;
                }else{
                    System.out.println("Operació impossible: div per 0");
                    break;
                }
            case 3:
                System.out.println("sumar()");
                break;
            case 4:
                System.out.println("restar()");
                break;
            default:
                System.out.println("Operació no disponible");
        }
    }

    public String givMeTheTrue1(boolean a, boolean b){
        if (a || b) {
            if (a) {
                return "a";
            } else {
                return "b";
            }
        } else {
            return "none";
        }
    }

    public String givMeTheTrue2(boolean a, boolean b){
        if (a) {
            return "a";
        } else if(b) {
            return "b";
        } else {
            return "none";
        }
    }




}
